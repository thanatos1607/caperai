import pandas as pd
import catboost as cb
from sklearn.model_selection import train_test_split

# загружаем данные

train = pd.read_csv('../../data/data.csv', index_col=0)

X = train.drop('radiant_win', axis=1)
y = train['radiant_win']

X_train, X_validation, y_train, y_validation = train_test_split(X, y, train_size=0.8, random_state=1234)

pool = cb.Pool(data=X, label=y)

model = cb.CatBoostClassifier(
    iterations=300,
    learning_rate=0.1,
    random_seed=63,
    custom_loss=['AUC', 'Accuracy'],
    use_best_model=True
)

model.fit(
    X_train, y_train,
    eval_set=(X_validation, y_validation),
    logging_level='Silent',
    plot=True
)

#params = {'iterations': 100, 'depth': 2, 'loss_function': 'MultiClass', 'classes_count': 3, 'logging_level': 'Silent'}

#scores = cb.cv(pool=pool, params=params)

print('Model is fitted:', model.is_fitted_)
print('Model params:', model.get_params())
print('Resulting tree count:', model.tree_count_)
