from loader import sql_query
from converter import convert_data
import pandas as pd


def update_query(match_id):
    return """
                SELECT
                matches.match_id,
                matches.radiant_team_id,
                matches.dire_team_id,
                matches.radiant_win,
                ((player_matches.player_slot < 128) = matches.radiant_win) win,
                player_matches.account_id,
                player_matches.kills,
                player_matches.deaths,
                player_matches.assists,
                player_matches.gold_per_min,
                player_matches.xp_per_min,
                player_matches.hero_damage,
                player_matches.tower_damage,
                player_matches.hero_healing   
                FROM matches
                JOIN player_matches USING(match_id)
                WHERE match_id > """ + str(match_id) + """
                ORDER BY matches.match_id DESC NULLS LAST
                """


def accordance_data(init_data, team_data):

    data = pd.DataFrame(sql_query(update_query(init_data['match_id'][0])))
    data.dropna(axis=0, inplace=True)
    data.reset_index(drop=True, inplace=True)

    data = convert_data(data)

    labels = ['kills', 'deaths', 'assists', 'gold_per_min', 'xp_per_min', 'hero_damage', 'tower_damage', 'hero_healing']

    for i in range(len(data)-1, -1, -1):

        if team_data.loc[team_data['team_id'] == data['radiant_team_id'][i]].empty == False:

            r_matches = team_data.loc[team_data['team_id'] == data['radiant_team_id'][i], 'matches']

            for key in labels:

                r_team_key = team_data.loc[team_data['team_id'] == data['radiant_team_id'][i], key]
                r_data_key = data['r_' + key][i]

                team_data.loc[team_data['team_id'] == data['radiant_team_id'][i], key] = (r_data_key + (r_team_key * r_matches)) / (r_matches + 1)
                data['r_' + key][i] = team_data.loc[team_data['team_id'] == data['radiant_team_id'][i], key]

            team_data.loc[team_data.team_id == data.loc[i].radiant_team_id, 'matches'] += 1

        else:

            new_row = [{'team_id': data['radiant_team_id'][i],
                       'matches': 1}]
            for key in labels:
                new_row[0][key] = data['r_' + key][i]

            team_data = pd.concat([pd.DataFrame(new_row), team_data], axis=0)
            team_data.reset_index(drop=True, inplace=True)

        if team_data.loc[team_data['team_id'] == data['dire_team_id'][i]].empty == False:

            d_matches = team_data.loc[team_data['team_id'] == data['dire_team_id'][i], 'matches']

            for key in labels:

                d_team_key = team_data.loc[team_data['team_id'] == data['dire_team_id'][i], key]
                d_data_key = data['d_' + key][i]

                team_data.loc[team_data['team_id'] == data['dire_team_id'][i], key] = (d_data_key + (d_team_key * d_matches)) / (d_matches + 1)
                data['d_' + key][i] = team_data.loc[team_data['team_id'] == data['dire_team_id'][i], key]

            team_data.loc[team_data.team_id == data.loc[i].dire_team_id, 'matches'] += 1

        else:

            new_row = [{'team_id': data['dire_team_id'][i],
                       'matches': 1}]
            for key in labels:
                new_row[0][key] = data['d_' + key][i]

            team_data = pd.concat([pd.DataFrame(new_row), team_data], axis=0)
            team_data.reset_index(drop=True, inplace=True)

    init_data = pd.concat([data, init_data], axis=0)
    init_data.reset_index(drop=True, inplace=True)

    team_data.to_csv('../data/team_data.csv')
    init_data.to_csv('../data/data.csv')


data = pd.read_csv('../data/data.csv', index_col=0)
team_data = pd.read_csv('../data/team_data.csv', index_col=0)

accordance_data(data, team_data)