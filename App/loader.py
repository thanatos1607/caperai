import requests as req

def sql_query(query):
    r = req.get('https://api.opendota.com/api/explorer?sql=' + query)
    print(r.status_code)
    return r.json().get('rows')