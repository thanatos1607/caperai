import pandas as pd


def convert_data(data):
    row_list = []
    exceptions = ['match_id', 'radiant_team_id', 'dire_team_id']
    labels = ['kills', 'deaths', 'assists', 'gold_per_min', 'xp_per_min', 'hero_damage', 'tower_damage', 'hero_healing']

    for k in range(0, len(data), 10):
        print('parsing players for match ' + str(int(k / 10)))
        players_rows = data.loc[range(k, k + 10)].to_dict('records')

        r_player_count = 1
        d_player_count = 1
        row = {}

        for f in labels:
            row['r_' + f] = 0
            row['d_' + f] = 0

        for i in range(0, 10):

            for ex in exceptions:
                row[ex] = players_rows[i].pop(ex)

            row['radiant_win'] = players_rows[i]['radiant_win']

            if players_rows[i].pop('radiant_win') == players_rows[i].pop('win'):
                row[('r%d_player' % r_player_count)] = players_rows[i].pop('account_id')
                r_player_count += 1

                for key in labels:
                    row['r_' + key] += players_rows[i][key]
            else:
                row[('d%d_player' % d_player_count)] = players_rows[i].pop('account_id')
                d_player_count += 1

                for key in labels:
                    row['d_' + key] += players_rows[i][key]

        row_list.append(row)

    return pd.DataFrame(row_list)