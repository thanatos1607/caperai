from loader import sql_query
from converter import convert_data
import pandas as pd


init_query = """
            SELECT
            matches.match_id,
            matches.radiant_team_id,
            matches.dire_team_id,
            matches.radiant_win,
            ((player_matches.player_slot < 128) = matches.radiant_win) win,
            player_matches.account_id,
            player_matches.kills,
            player_matches.deaths,
            player_matches.assists,
            player_matches.gold_per_min,
            player_matches.xp_per_min,
            player_matches.hero_damage,
            player_matches.tower_damage,
            player_matches.hero_healing   
            FROM matches
            JOIN player_matches USING(match_id)
            WHERE TRUE 
            ORDER BY matches.match_id DESC NULLS LAST
            """

def create_tables(data):

    data = convert_data(data)

    radiant_team_list = pd.DataFrame(data['radiant_team_id'].unique())
    dire_team_list = pd.DataFrame(data['dire_team_id'].unique())

    team_data = pd.DataFrame(pd.concat([radiant_team_list, dire_team_list], axis=0))
    team_data.rename(columns={0: 'team_id'}, inplace=True)
    team_data.drop_duplicates(inplace=True, keep='first')
    team_data.reset_index(drop=True, inplace=True)

    team_data['matches'] = 0

    labels = ['kills', 'deaths', 'assists', 'gold_per_min', 'xp_per_min', 'hero_damage', 'tower_damage', 'hero_healing']

    for key in labels:
        team_data[key] = 0

    # Скрипт на ночь ;)

    for i in range(len(data) - 1, -1, -1):
        print(i)
        team_data.loc[team_data['team_id'] == data['radiant_team_id'][i], 'matches'] += 1
        team_data.loc[team_data['team_id'] == data['dire_team_id'][i], 'matches'] += 1
        for key in labels:
            team_data.loc[team_data['team_id'] == data['radiant_team_id'][i], key] += data['r_' + key][i]
            team_data.loc[team_data['team_id'] == data['dire_team_id'][i], key] += data['d_' + key][i]
            data['r_' + key][i] = team_data.loc[team_data['team_id'] == data['radiant_team_id'][i], key] / team_data.loc[team_data['team_id'] == data['radiant_team_id'][i], 'matches']
            data['d_' + key][i] = team_data.loc[team_data['team_id'] == data['dire_team_id'][i], key] / team_data.loc[team_data['team_id'] == data['dire_team_id'][i], 'matches']

    for key in labels:
        team_data[key] /= team_data['matches']

    data.to_csv('../data/data.csv')
    team_data.to_csv('../data/team_data.csv')


df = pd.DataFrame(sql_query(init_query))
df.dropna(axis=0, inplace=True)
df.reset_index(drop=True, inplace=True)

create_tables(df)

